package pdfreadernew;

import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestCases {
	TSVReader treader = new TSVReader();
	PDF	preader = new PDF();
	RobotClass robotClass = new RobotClass();
	@Test
	public void tst_TC01_RetailerCustomerDiscount_25percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		robotClass.typeNew();
//		String pickinglistPDFText = preader.pdf("pickinglist1.pdf");
//    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
//		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[3],"The product is not matching");
//		
//		String invoicePDFText = preader.pdf("invoice1.pdf");
//    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
//		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[3],"The product is not matching");
	}
	/*
	@Test
	public void tst_TC02_RetailerCustomerDiscount_0percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		
		String pickinglistPDFText = preader.pdf("pickinglist2.pdf");
    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[5],"The product is not matching");
		
		String invoicePDFText = preader.pdf("invoice2.pdf");
    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[5],"The product is not matching");
	}
	
	@Test
	public void tst_TC09_LoyaltyDiscount_0percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		
		String pickinglistPDFText = preader.pdf("pickinglist3.pdf");
    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[9],"The product is not matching");
		
		String invoicePDFText = preader.pdf("invoice3pdf");
    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[9],"The product is not matching");
	}
	
	@Test
	public void tst_TC_11_LoyaltyDiscount5percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		
		String pickinglistPDFText = preader.pdf("pickinglist4.pdf");
    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[11],"The product is not matching");
		
		String invoicePDFText = preader.pdf("invoice4.pdf");
    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[11],"The product is not matching");
	}
	
	@Test
	public void tst_TC_013_Loyalty_Discount_of_10percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		
		String pickinglistPDFText = preader.pdf("pickinglist5.pdf");
    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[13],"The product is not matching in packing list");
		
		String invoicePDFText = preader.pdf("invoice5.pdf");
    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[13],"The product is not matching in invoice");
	}
	
	@Test
	public void tst_TC15_LoyaltyDiscount15percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		
		String pickinglistPDFText = preader.pdf("pickinglist6.pdf");
    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[17],"The product is not matching in packing list");
		
		String invoicePDFText = preader.pdf("invoice6.pdf");
    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[17],"The product is not matching in invoice");
	}
	
	@Test
	public void tst_TC_017_LoyaltyDiscount20percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		
		String pickinglistPDFText = preader.pdf("pickinglist7.pdf");
    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[19],"The product is not matching in packing list");
		
		String invoicePDFText = preader.pdf("invoice7.pdf");
    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[19],"The product is not matching in invoice");
	}
	
	@Test
	public void tst_TC_30_VisionSourceDiscount_20percent() throws Exception{
		List<String> product = treader.tsv("OrderDetails.tsv");
		
		String pickinglistPDFText = preader.pdf("pickinglist8.pdf");
    	System.out.println("ProductPrice "+ pickinglistPDFText.split("\n")[20].split(" ")[0]);
		Assert.assertEquals(pickinglistPDFText.split("\n")[20].split(" ")[0],product.get(1).split("\"")[25],"The product is not matching in packing list");
		
		String invoicePDFText = preader.pdf("invoice8.pdf");
    	System.out.println("ProductPrice "+invoicePDFText.split("\n")[23].split(" ")[0]);
		Assert.assertEquals(invoicePDFText.split("\n")[23].split(" ")[0],product.get(1).split("\"")[25],"The product is not matching in invoice");
	}*/

}
