package pdfreadernew;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class TSVReader {

	public List<String> tsv(String orderDetails) throws Exception {
		// TODO Auto-generated method stub
		 BufferedReader TSVFile = new BufferedReader(new FileReader(orderDetails));
	        List<String>dataArray = new ArrayList<String>() ;
	        String dataRow = TSVFile.readLine();
	        while (dataRow != null){
	            dataArray.add(dataRow);
	            dataRow = TSVFile.readLine();
	        }
	        TSVFile.close();    
	        for(String item:dataArray){
	            //System.out.println(item);
	        }
	        return (dataArray);
	    }       

	}

