package pdfreadernew;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;

import java.io.IOException;

public class PDF {
	
    public String pdf(String test){
        PdfReader reader;
        try {
            reader = new PdfReader(test);
            String pickUpText = PdfTextExtractor.getTextFromPage(reader, 1);
            reader.close();
            System.out.println(pickUpText);
        	return pickUpText;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}